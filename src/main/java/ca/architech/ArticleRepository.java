package ca.architech;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArticleRepository extends MongoRepository<Article, String>, ArticleRepositoryCustom {
    @Query(value = "{'title': {$regex : '?0', $options: 'i'}}")
    public List<Article> findByTitleLike(@Param("title") String title);
}