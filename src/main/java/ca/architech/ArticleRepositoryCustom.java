package ca.architech;

import java.util.List;

public interface ArticleRepositoryCustom {
    public List<Article> findByTitleReverse(String title);
}
