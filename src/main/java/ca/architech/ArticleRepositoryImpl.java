package ca.architech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.regex.Pattern;

public class ArticleRepositoryImpl implements ArticleRepositoryCustom {
    @Autowired
    MongoTemplate template;

    @Override
    public List<Article> findByTitleReverse(String title) {
        Criteria criteria = Criteria.where("title").regex(Pattern.compile(new StringBuilder(title).reverse().toString(), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
        return template.find(Query.query(criteria), Article.class);
    }
}
