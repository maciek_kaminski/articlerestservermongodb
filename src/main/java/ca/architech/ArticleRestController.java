package ca.architech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ArticleRestController  {

    @Autowired
    ArticleRepository repository;

    @RequestMapping(value = "/searchByTitleReverse/{title}", method = RequestMethod.GET)
    public List<Article> findByTitleReverse(@PathVariable String title) {
        return repository.findByTitleReverse(title);
    }
}
