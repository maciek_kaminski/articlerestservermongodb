package ca.architech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleRestServerApplication implements CommandLineRunner {

    @Autowired
    ArticleRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(ArticleRestServerApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        repository.deleteAll();

        // save a couple of Articles
        repository.save(new Article("Aim to Kill", "When disgraced former cop Alexandra Morgan is shot saving the life of California's Lieutenant Governor, she doesn't expect to be caught in the middle of a deadly conspiracy, an FBI sting, an old rivalry ... and cold-blooded murder.  When the shooter gets away after Alex pursues him, she gives her statement to the police, but is skeptical that LG Travis Hart was the actual target. The lead detective—Alex’s..."));
        repository.save(new Article("Be Afraid", "The Fear Is TerrifyingWhen police rescue five-year-old Jenna Thompson from the dark closet where she's been held captive for days, they tell her she's a lucky girl. Compared to the rest of her family, it's true. But even with their killer dead of an overdose, Jenna is still trying to find peace twenty-five years later. But The TruthOn leave from her forensic artist job, Jenna returns to Nashville, the city where she lost so much..."));
        repository.save(new Article("How I Lost You", "A brilliant debut psychological crime novel, for fans of THE WICKED GIRLS.They told her she killed her son. She served her time. But what if they lied?I have no memory of what happened but I was told I killed my son. And you believe what your loved ones, your doctor and the police tell you, don't you? My name is Emma Cartwright. Three years ago I was Susan Webster, and I murdered my twelve-week-old son Dylan..."));
        repository.save(new Article("Lost Girls", "Two girls go missing.  Only one will return.\n" +
                "The couple that offers the highest amount will see their daughter again.  The losing couple will not. Make no mistake.  One child will die."));

        // fetch all Articles
        System.out.println("Articles found with findAll():");
        System.out.println("-------------------------------");
        for (Article article : repository.findAll()) {
            System.out.println(article);
        }
        System.out.println();

        // fetch Articles by title
        System.out.println("Article found with findByTitle('Afraid'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByTitleLike("Afraid"));

        System.out.println();

        // fetch Articles by reverse title
        System.out.println("Article found with findByTitleReverse('miA'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByTitleReverse("miA"));System.out.println();

        System.out.println("Article case insensitive found with findByTitleReverse('woh'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByTitleReverse("woh"));

    }
}
